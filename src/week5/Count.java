class Count{
    int jie,N,M;
    public void sum(int n,int m) throws WrongException {     //计算结果
        if(n<=0||m<0||n<m){
            throw new WrongException("确保n>0,m>=0且n<=m！"); //当输入错误时抛出异常
        }
        N = Jiecheng(n);
        M = Jiecheng(n-m);
    }
    public int Jiecheng(int a){  //计算阶乘
        jie=1;
        for(int i=1;i<=a;i++){
            jie=jie*i;
        }
        return jie;
    }
    public int getTotal() {  //返回计算值
        return (N/M);
    }

}
