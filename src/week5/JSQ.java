import java.io.*;
import java.util.*;
public class JSQ {
    public static void main(String args[]) {
        Count count=new Count();

        System.out.println("请输入总数n");//输入总的个数
        Scanner a = new Scanner(System.in);
        int n = a.nextInt();
        System.out.println("请输入个数m");//输入要选的个数
        Scanner b = new Scanner(System.in);
        int m = b.nextInt();
        try {
            count.sum(n,m);
            int total= count.getTotal();
            System.out.println("结果为"+total);
        }
        catch(WrongException e)
        {
            System.out.println("输入出现错误");
            System.out.println(e.Mess());
        }
    }
}
class Count{
    int jie,N,M;
    public void sum(int n,int m) throws WrongException {     //计算结果
        if(n<=0||m<0||n<m){
            throw new WrongException("确保n>0,m>=0且n<=m！"); //当输入错误时抛出异常
        }
        N = Jiecheng(n);
        M = Jiecheng(n-m);
    }
    public int Jiecheng(int a){  //计算阶乘
        jie=1;
        for(int i=1;i<=a;i++){
            jie=jie*i;
        }
        return jie;
    }
    public int getTotal() {  //返回计算值
        return (N/M);
    }

}
class WrongException extends Exception {   //自定义异常
    String Message;
    public WrongException(String s) {
        Message = s;
    }
    public String Mess( ){
        return Message;
    }
}
