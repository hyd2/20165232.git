public class Complex{
    private double r;
    private double i;
    public Complex(double r, double i) {
        this.r = r;
        this.i = i;
    }

    public static double getRealPart(double r) {
        return r;
    }

    public static double getImagePart(double i) {
        return i;
    }

    public Complex ComplexAdd(Complex c) {
        return new Complex(r + c.r, i + c.i);
    }
    public Complex ComplexSub(Complex c) {
        return new Complex(r - c.r, i - c.i);
    }
    public Complex ComplexMulti(Complex c) {
        return new Complex(r * c.r - i * c.i, r * c.i + i * c.r);
    }
    public Complex ComplexDiv(Complex c) {
        return new Complex((r * c.i + i * c.r)/(c.i * c.i + c.r * c.r), (i * c.i + r * c.r)/(c.i * c.i + c.r * c.r));
    }

    public String toString() {
        String s = " ";
        if (i > 0)
            s =  r + "+" + i + "i";
        if (i == 0)
            s =  r + "";
        if (i < 0)
            s = r + " " + i + "i";
        return s;
    }
}
