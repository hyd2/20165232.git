public class FinallyDemo {
public static void main(String[] args) {
   System.out.print(test(true));
}
static int test(boolean flag) {
   try {
       if (flag) {
           return 1;
       }
   } finally {
       System.out.print("finally…");
   }
   return 0;
}
}
