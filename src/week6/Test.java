import java.util.*;
class CPU {
       int speed;
       CPU(){
       }
       CPU(int speed){
        setSpeed(speed);
       }

       int getSpeed(){
             return speed;
       }
        public void setSpeed(int speed){
            this.speed=speed; 
        }
       public String toString (){
          String str = super.toString();
           return "cpu="+str;
          }
       public boolean equals(Object other){       
                 if (this == other)                                      
                       return true;
                  if(other == null)         
                     return false;
                  if( other instanceof CPU){
                        CPU p = (CPU) other;
                        if (p.speed==this.speed)
                               return true;
                     } 
                       return false;
                           
       }
}
class HardDisk{
         int amount;
          HardDisk(){
          }
          HardDisk(int amount){
            setAmount(amount);
         }
         int getAmount(){
              return amount;
         }
         public void setAmount(int amount){
                this.amount = amount;
         }
         public String toString (){
          String str = super.toString();
           return "HD="+str;
          }
          public boolean equals(Object other){       
                 if (this == other)                                      
                       return true;
                  if(other == null)         
                     return false;
                  if( other instanceof HardDisk){
                      HardDisk  p = (HardDisk) other;
                        if (p.amount==this.amount)
                               return true;
                     } 
                       return false;
                           
       }
}
class PC{
       CPU cpu;
        HardDisk HD;
        PC(){
        }
        PC(HardDisk HD,CPU cpu){
             this.HD=HD;
             this.cpu=cpu; 
              show();       
        }        
        void setCPU(CPU cpu){
               this.cpu=cpu;
         }
         void setHardDisk(HardDisk HD){
              this.HD=HD;
          } 
          void show(){
              System.out.println("CPU的速度:"+cpu.getSpeed());
              System.out.println("硬盘容量:"+HD.getAmount());          
          }
          public String toString (){
          String str = super.toString();
           return "pc="+str;
          }
}
public class Test{
          public static void main(String args[]){
                  CPU cpu= new CPU(2200);
                  HardDisk HD = new HardDisk(200);
                  PC pc = new PC();
                  pc.setCPU(cpu);
                  pc.setHardDisk(HD);
                  pc.show();
                  CPU cpu1;
                  HardDisk HD1;
                  HD1 = new HardDisk(200);
                  cpu1 = new CPU(2200);
                  pc = new PC(HD1 ,cpu1);
                  System.out.println(cpu.toString());
                  System.out.println(HD.toString());
                  System.out.println(pc.toString());
                  System.out.println("cpu与cpu1的变量speed值相等?"+cpu.equals(cpu1));
                  System.out.println("HD与HD1的变量amount值相等?"+HD.equals(HD1));
      
                  
          }
}
